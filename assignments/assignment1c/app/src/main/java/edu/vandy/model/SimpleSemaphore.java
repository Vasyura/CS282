package edu.vandy.model;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

/**
 * This class define a counting semaphore with "fair" semantics.  Grad
 * students must implement this class using a Java ReentrantLock and
 * ConditionObject, whereas undergrad students must implement this
 * class using Java built-in monitor object features.
 */
public class SimpleSemaphore {
    /**
     * Define a count of the number of available permits.
     */
    // TODO - you fill in here.  Make sure that this field will ensure
    // its values aren't cached by multiple threads..
    //private volatile int count;

    private volatile int availableCount;
    /**
     * Grad students define a ReentrantLock to protect critical
     * sections.
     */
    // TODO - you fill in here
    ReentrantLock lock;
    
    /**
     * Grad students define a Condition that's used to wait while the
     * number of permits is 0.
     */
    // TODO - you fill in here
    Condition notFull;

    /**
     * Constructor initialize the fields.
     */
    public SimpleSemaphore (int permits) {
        // TODO -- you fill in here. Grad students make sure the
        // ReentrantLock has "fair" semantics.
        availableCount = permits;
        lock = new ReentrantLock(false);
        notFull = lock.newCondition();
    }

    /**
     * Acquire one permit from the semaphore in a manner that can be
     * interrupted.
     */
    public void acquire()
        throws InterruptedException {
        // TODO -- you fill in here.
        try {
            lock.lockInterruptibly();
            while(availableCount == 0) {
                notFull.await();
            }
            availableCount --;
            // ... method body
        }finally {
            lock.unlock();
        }
    }

    /**
     * Acquire one permit from the semaphore in a manner that cannot
     * be interrupted.  If an interrupt occurs while this method is
     * running make sure to set the interrupt state when the thread
     * returns from this method.
     */
    public void acquireUninterruptibly() {
        // TODO -- you fill in here.

        boolean interrupted = false;
        try {
            lock.lock();
            while(availableCount == 0) {
                notFull.await();
            }
            availableCount --;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Return one permit to the semaphore.
     */
    public void release() {
        // TODO -- you fill in here.
        try {
            lock.lock();
            availableCount ++;
            notFull.signalAll();
        }finally {
            lock.unlock();
        }
    }

    /**
     * Returns the current number of permits.
     */
    public int availablePermits() {
        // TODO -- you fill in here.
        return availableCount;
    }
}
